Distr-Switch-0#show startup-config
Using 1254 bytes
!
version 12.1
no service timestamps log datetime msec
no service timestamps debug datetime msec
no service password-encryption
!
hostname Distr-Switch-0
!
enable secret 5 $1$mERr$OF2LMESBV/L.9KdoJf2CI/
!
!
!
ip ssh version 2
ip domain-name cisco.com
!
username ciaup privilege 1 password 0 ciaup
!
!
!
spanning-tree mode pvst
spanning-tree extend system-id
!
interface GigabitEthernet0/1
 switchport mode trunk
!
interface GigabitEthernet1/1
 switchport mode trunk
!
interface GigabitEthernet2/1
!
interface GigabitEthernet3/1
!
interface GigabitEthernet4/1
!
interface GigabitEthernet5/1
!
interface GigabitEthernet6/1
!
interface GigabitEthernet7/1
!
interface GigabitEthernet8/1
!
interface GigabitEthernet9/1
!
interface Vlan1
 ip address 10.1.0.4 255.255.255.0
!
interface Vlan2
 description vlan-2-pc
 ip address 10.2.0.4 255.255.255.0
!
interface Vlan11
 description vlan-11-servers
 ip address 10.11.0.4 255.255.255.0
!
interface Vlan100
 description vlan-100-wireless
 ip address 10.100.0.4 255.255.255.0
!
banner motd ^C
SOLO ACCESO A PERSONAL AUTORIZADO^C
!
!
!
line con 0
 password sanjose
 login
!
line vty 0 4
 password sanjose
 login local
 transport input ssh
line vty 5 15
 password sanjose
 login local
 transport input ssh
!
!
!
!
end
