# Practica 3

Integrantes:

* Rodrigo Arévalo Gaytan - `317285880`
* Diana Berenice Hernández Alonso - `317183425`
* Fausto David Hernández Jasso - `317000928`
* Julio César Torres Escobedo - `317336267`


## Actividad

Topología de la red:


## Equipos en vLAN

| Equipos  | vLAN |
| ----------- | ----------- |
| PC0  | vLAN 2  |
| PC1  | vLAN 2  |
| Server0  | vLAN 11  |
| Server1  | vLAN 11  |
| Smartphone0  | vLAN 100  |
| Smartphone1  | vLAN 100  |
| TabletPC1  | vLAN 100  |
| TabletPC0  | vLAN 100  |
| Laptop1  | vLAN 100  |

## Conexión de cada switch

| Switch | Conexión | Local Intrfce | Holdtme | Capability | Platform | Port ID |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| acceso-Piso1 |Distr-Switch-1 | Gig 0/1 | 146 | S | PT3000 | Gig 2/1 |
| Distribucion-P1 | Switch-Core | Gig 0/1 | 177 |   | 3560 | Fas 0/2 |
| Distribucion-P1 | CIAUPP1 | Gig 2/1 | 175 | S | PT3000 | Gig 0/1 |

## Lista de direcciones IP
- `10.0.0.1`
- `10.0.0.2`
- `10.0.100.1`
- `10.0.100.2`
- `10.0.100.3`

## Ruteo del switch

| Switch  | Ruteo |
| ----------- | ----------- |
| A  | A  |
| A  | A  |

## Lista de Imágenes

| Lista de Imágenes
|:----------------------:|
| ![Topologia Planta Baja.](img/TopologiaRedPB.png)
| Topología propuesta para la red del CIAUP de la Planta Baja.
| ![Topologia Piso 1.](img/TopologiaRedP1.png)
| Topología propuesta para la red del CIAUP del Piso 1.
